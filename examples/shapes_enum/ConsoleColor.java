
public enum ConsoleColor {
    BLACK('0'),
    RED('1'),
    GREEN('2'),
    WHITE('7');

      private char code = '0';

      ConsoleColor(char code){
        this.code = code;
      }

      public char code(){
        return code;
      }

      public String apply(String s) {
          return "\u001B[3" + code() + "m" + s + "\u001B[0m";
      }
}
