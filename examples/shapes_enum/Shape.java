public abstract class Shape {
    private ConsoleColor fillColor, strokeColor;
    private int strokeWidth;

    public Shape() {
        this.fillColor = ConsoleColor.BLACK;
        this.strokeColor = ConsoleColor.BLACK;
    }
    public Shape(ConsoleColor fillColor, ConsoleColor strokeColor, int strokeWidth) {
        this.fillColor = fillColor;
        this.strokeColor = strokeColor;
        this.strokeWidth = strokeWidth;
    }

    public void setFillColor(ConsoleColor b) { fillColor = b; }
    public void setStrokeColor(ConsoleColor b) { strokeColor = b; }
    public void setStrokeWidth(int w) { strokeWidth = w; }

    public ConsoleColor getFillColor() { return fillColor; }
    public ConsoleColor getStrokeColor() { return strokeColor; }
    public int getStrokeWidth() { return strokeWidth; }

    abstract public boolean contains(int x, int y);

    public boolean onStroke(int x, int y) {
        if(contains(x, y)) return false;

        for(int dx = -strokeWidth; dx <= strokeWidth; ++dx) {
            for(int dy = -strokeWidth; dy <= strokeWidth; ++dy) {
                if(contains(x + dx, y + dy)) return true;
            }
        }
        return false;
    }
    @Override
    public String toString() { return "Shape"; }
}
