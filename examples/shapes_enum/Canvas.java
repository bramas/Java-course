

public class Canvas {
    private Shape[] shapes;
    private int width, height, nbShapes;

    public Canvas(int width, int height) {
        this.width = width;
        this.height = height;
        nbShapes = 0;
        shapes = new Shape[50];
    }
    public void addShape(Shape s) {
        shapes[nbShapes++] = s;
    }
    public ConsoleColor getColorAt(int x, int y) {
        ConsoleColor color = ConsoleColor.WHITE;
        for(int i = 0; i < nbShapes; ++i){
            if(shapes[i].contains(x, y)) {
                color = shapes[i].getFillColor();
            } else if(shapes[i].onStroke(x, y)) {
                color = shapes[i].getStrokeColor();
            }
        }
        return color;
    }
}
