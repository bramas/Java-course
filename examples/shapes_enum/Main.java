
class Main {
    public static void main(String[] args) {
        Canvas canvas = new Canvas(80, 20);
        Shape shape = new Circle(10, 5, 5);

        canvas.addShape(shape);
        shape.setStrokeWidth(2);
        shape.setFillColor(ConsoleColor.RED);
        shape.setStrokeColor(ConsoleColor.GREEN);

        shape = new Rectangle(40, 10, 20, 2);

        canvas.addShape(shape);
        shape.setStrokeWidth(1);
        shape.setFillColor(ConsoleColor.GREEN);
        shape.setStrokeColor(ConsoleColor.RED);
        shape = new Rectangle(50, 8, 20, 5);

        canvas.addShape(shape);
        shape.setStrokeWidth(0);
        shape.setFillColor(ConsoleColor.BLACK);


        for(int y = 0; y < 20; ++y) {
            for(int x = 0; x < 80; ++x) {
                System.out.print(canvas.getColorAt(x,y).apply("█"));
            }
            System.out.print("\n");
        }

    }
}
