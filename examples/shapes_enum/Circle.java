
public class Circle extends Shape {
    private int x, y, radius;
    public Circle(int x, int y, int radius) {
        this.x = x;
        this.y = y;
        this.radius = radius;
    }
    @Override
    public boolean contains(int x, int y) {
        return (x-this.x)*(x-this.x) + (y - this.y)*(y - this.y) < radius*radius;
    }

}
