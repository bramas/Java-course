
class Main {
    public static void main(String[] args) {
        //Shape shape = new Rectangle(10, 5, 20, 2);
        Shape shape = new Circle(10, 5, 5);
        
        shape.setStrokeWidth(2);
        shape.setFillColor('2');
        shape.setStrokeColor('1');
        for(int y = 0; y < 20; ++y) {
            for(int x = 0; x < 80; ++x) {
                if(shape.contains(x, y)) {
                    System.out.print("\u001B[3"+shape.getFillColor()+"m"+"█"+"\u001B[0m");
                }
                else if(shape.onStroke(x,y)) {
                    System.out.print("\u001B[3"+shape.getStrokeColor()+"m"+"█"+"\u001B[0m");
                }
                else System.out.print("█");
            }
            System.out.print("\n");
        }
    }
}
