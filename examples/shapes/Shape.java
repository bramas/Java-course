public abstract class Shape {
    private char fillColor, strokeColor;
    private int strokeWidth;


    public Shape() {

    }
    public Shape(char fillColor, char strokeColor, int strokeWidth) {
        this.fillColor = fillColor;
        this.strokeColor = strokeColor;
        this.strokeWidth = strokeWidth;
    }

    public void setFillColor(char b) { fillColor = b; }
    public void setStrokeColor(char b) { strokeColor = b; }
    public void setStrokeWidth(int w) { strokeWidth = w; }

    public char getFillColor() { return fillColor; }
    public char getStrokeColor() { return strokeColor; }
    public int getStrokeWidth() { return strokeWidth; }

    abstract public boolean contains(int x, int y);

    public boolean onStroke(int x, int y) {
        if(contains(x, y)) return false;

        for(int dx = -strokeWidth; dx <= strokeWidth; ++dx) {
            for(int dy = -strokeWidth; dy <= strokeWidth; ++dy) {
                if(contains(x + dx, y + dy)) return true;
            }
        }
        return false;
    }
    @Override
    public String toString() { return "Shape"; }
}
