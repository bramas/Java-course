import java.io.*;

class DoubleReader extends FilterReader {

    private int lastChar = -1;
    public DoubleReader(Reader r) {
        super(r);
    }
    @Override
     public int read() throws IOException {
         if(lastChar != -1) {
             int r = lastChar;
             lastChar = -1;
             return r;
         }
         lastChar = super.read();
         return lastChar;
     }

     @Override
     public int read(char[] cbuf,
                     int off,
                     int len)
              throws IOException {
        if(len == 0) { return 0;}
        
        if(len == 1 || lastChar != -1) {
            int r = read();
            if(r == -1) {
                return -1;
            }
            cbuf[off] = (char)r;
            return 1;
        }

        len /= 2;
        int ok = super.read(cbuf, off, len);
        if(ok == -1) {
            return -1;
        }

        for(int idx = ok - 1; idx >= 0; idx --) {
            cbuf[off + idx*2] = cbuf[off + idx*2 + 1] = cbuf[off + idx];
        }

        return ok*2;
      }
}
