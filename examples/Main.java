import java.io.*;
import javax.crypto.*;
import java.security.SecureRandom;

import javax.crypto.spec.*;

import java.util.zip.GZIPInputStream;
import java.util.zip.GZIPOutputStream;

import java.nio.file.*;
import java.util.stream.*;

public class Main {
  public static void main4(String[] args) throws Exception {
      // Lancement du programme say (Mac Os)
      Process p = Runtime.getRuntime().exec("say");
      // Récupération du flux d’entrée standard du programme
      // ATTENTION : Pour nous c’est un flux de sortie (out)
      OutputStream os = p.getOutputStream();

      // Utilisation du flux : comme avec tout OutputStream
      os.write("bonjour, comment ça va?".getBytes());

      // Fermeture du flux (=> arrêt du programme beep)
      os.close();
  }

  public static void main2(String[] args)  throws Exception {

      byte[] keyBytes = "1234123412341234".getBytes();
      final SecretKey key = new SecretKeySpec(keyBytes, "AES");

      final byte[] ivBytes = new byte[] { 0x00, 0x01, 0x02, 0x03, 0x04, 0x05, 0x06, 0x07, 0x08, 0x09, 0x0a, 0x0b, 0x0c, 0x0d, 0x0e, 0x0f };
      final IvParameterSpec IV = new IvParameterSpec(ivBytes);

      Cipher c = Cipher.getInstance("AES/CFB8/NoPadding");
      c.init(Cipher.ENCRYPT_MODE, key, IV);



      OutputStream os = new GZIPOutputStream(
                        new CipherOutputStream(
                        new FileOutputStream("test.bin"), c));
      os.write((byte)1);
      os.write((byte)15);
      os.write((byte)16);
      os.flush();
      os.close();


      c = Cipher.getInstance("AES/CFB8/NoPadding");
      c.init(Cipher.DECRYPT_MODE, key, IV);


      InputStream is = new GZIPInputStream(
      new CipherInputStream(
      new FileInputStream("test.bin")
      , c));
      int x;
      while ( (x = is.read()) != -1 )
      {
          System.out.println(x);
      }
      is.close();
  }

  public static void main3(String[] args) throws IOException {

            Reader r = new DoubleReader(
                              new FileReader("test.txt"));

          /*int x;
          while ( (x = r.read()) != -1 )
          {
              System.out.print((char)x);
          }*/
          System.out.print((char)r.read());

          char[] cbuf = new char[50];
          while(-1 != r.read(cbuf))
            System.out.print(cbuf);
          r.close();
  }
  public static void main(String[] args) throws IOException {

      Stream<String> lines = Files.lines(Paths.get("Main.java"));

      lines
      .filter((s) -> s.startsWith("  p"))
      .forEach(System.out::println);
  }
}
