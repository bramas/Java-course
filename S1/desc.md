## Classe FeuTricolor

1. Créer une class représentant un feu tricolor.
   Contient la couleur du feu, deux constructeurs, une méthode next qui change la couleur.

1. Creer une classe Main qui créer et tester un feu.

1. Utilisez une énumération.

1. Créer une class représentant un carrefour (uniquement deux feux, horizontal et vertical) avec un temps d'attente entre deux changment de feu.
   On utilisera la méthode `Thread.sleep(int)` pour simuler l'attente.

1. Creer une classe Main qui créer et tester votre carrefour.


## Classe Personne

1. On va créer une classe qui représente une personne, avec un nom, un sexe et les parents (ou null si inconnus).

1. Avant de créer la class Personne, créer une class TestPersonne qui teste la classe personne.

1. Créer la classe Personne.
