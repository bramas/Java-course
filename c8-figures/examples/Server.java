import java.util.*;
import java.net.*;
import java.io.*;
class Server {

    public static void main(String[] args) throws Exception {

        int port = 3014;
        ServerSocket serverSocket = new ServerSocket(port);
        System.out.println("waiting for connexions in port "+port);

        while(true) {

            Socket clientSocket = serverSocket.accept();
            PrintWriter out = new PrintWriter(clientSocket.getOutputStream(), true);
            BufferedReader in =
                new BufferedReader(
                new InputStreamReader(clientSocket.getInputStream())
                );
            System.out.println("connection of "+clientSocket.getRemoteSocketAddress());

            String inputLine = "";
            while ((inputLine = in.readLine()) != null) {
                    System.out.println(inputLine);
                    if (inputLine.equals("Bye"))
                    {
                            out.println("Bye");
                            break;
                    }
                    out.println("You said "+outputLine);
            }
            System.out.println("disconnection of "+clientSocket.getRemoteSocketAddress());

            out.close();
            in.close();
            clientSocket.close();

        }
    }
}
