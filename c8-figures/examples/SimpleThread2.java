class TacheDeFond implements Runnable {
  private int iter;

  TacheDeFond(int iter) {
    this.iter = iter;
  }

  @Override
  public void run() {
    while(iter-->0) {
      System.out.println(Thread.currentThread().getName()+": "+iter);
    }
  }
}

class SimpleThread2 {
  public static void main(String[] args) throws Exception {
      Runnable t = new TacheDeFond(50);
      Thread t1 = new Thread(t,"t1");
      t1.start();
      Thread t2 = new Thread(t,"t2");
      t2.start();
      System.out.println(Thread.currentThread().getName()+" : Les threads ont été lancés");
      t1.join();
      t2.join();
      System.out.println("Les threads sont terminés");
  }
}
