import java.util.concurrent.*;

class SimpleCallable {
  public static void main(String[] args) throws Exception {
    Runnable task = ()-> {
      int i = 0;
      try {
        while(true) {
          System.out.println(i++);
          Thread.sleep(500);
          if(i>10) break;
         }
        }
        catch(Exception e) { System.out.println("Cancelled"); }
      };
    Thread t = new Thread(task);
    t.start();
    Thread.sleep(2000);
    t.interrupt();
  }
}
