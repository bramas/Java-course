import java.lang.ProcessBuilder.*;
import java.io.*;

class ExampleRuntime {
    public static void main(String[] args)  {
        try {
            Runtime monApplication = Runtime.getRuntime();

            Process p1 = monApplication.exec("cat index.md");
            Process p2 = monApplication.exec("python script.py");
            try {
                ProcessBuilder pb = new ProcessBuilder("espeak", "Text to speak");
                Process p3 = pb.start();
            } catch (Exception e) {
                System.out.println(e);
            }
            ProcessBuilder pb = new ProcessBuilder("say", "Bonjour tout le monde!");
            Process p4 = pb.start();

            Process[] processes = {p1, p2};
            for(Process p : processes) {
                System.out.println("---- Sortie standard du process "+p+" ----");
                BufferedReader in = new BufferedReader(new InputStreamReader(p.getInputStream()));

                String line = null;
                while ((line = in.readLine()) != null) {
                      System.out.println(line);
                }
                System.out.println("---- Sortie erreur du process "+p+" ----");

                in = new BufferedReader(new InputStreamReader(p.getErrorStream()));
                while ((line = in.readLine()) != null) {
                      System.out.println(line);
                }
            }

            // Waits for the command to finish.
            p1.waitFor();
            p2.waitFor();

        } catch (Exception e) {
            System.out.println(e);
        }
    }
}
