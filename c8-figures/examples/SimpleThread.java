class TacheDeFond implements Runnable {
  @Override
  public void run() {
    int i = 40;
    while(i-->0) {
      System.out.println(i);
    }
  }
}

class SimpleThread {
  public static void main(String[] args) throws Exception {
      Runnable t = new TacheDeFond();
      Thread t1 = new Thread(t);
      t1.start();
      Thread t2 = new Thread(t);
      t2.start();
      System.out.println("Les threads ont été lancés");
      t1.join();
      t2.join();
      System.out.println("Les threads sont terminés");
  }
}
