class BusyWait {
  private static void longComputation() {
    long timeToWait = 1000 + System.currentTimeMillis();
    while(System.currentTimeMillis() < timeToWait);
  }
  public static void main(String[] args) throws Exception {
    Thread t1 = new Thread(() -> {
      int i = 10;
      while(i-->0) {
        System.out.println("t1: "+i);
        longComputation();
      }
    });
    t1.start();

    Thread t2 = new Thread(() -> {
      int i = 10;
      try {
        while(i-->0) {
          System.out.println("t2: "+i);
          longComputation();
          Thread.sleep(0);
        }
      } catch(Exception e) {
        System.out.println(e);
      }
    });
    t2.start();

    Thread.sleep(3000);
    t1.interrupt();
    t2.interrupt();
    System.out.println("trying to stop the thread");
  }
}
