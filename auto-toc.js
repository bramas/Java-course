$(function() {
	var sections = [];

	$('section.toc-pause h1').each(function() {
		$(this).hide();
		sections.push($(this).html());
	});
	var i = 0;
	$('section.toc-pause').each(function() {
		var ul = $('<ul class="toc pause"></ul>');
		for(var j in sections) {
			var el = $('<li>' + sections[j] + '</li>');
			if(i == j) el.addClass('infocus');
			ul.append(el);
		}
		$(this).append(ul);
		++i;
	});
	console.log('auto toc:');
	console.log(sections);
})