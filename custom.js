
// More info https://github.com/hakimel/reveal.js#configuration
Reveal.initialize({
    history: true,
    slideNumber: true,
    transition: 'appear',

    // More info https://github.com/hakimel/reveal.js#dependencies
    dependencies: [
        { src: 'plugin/markdown/marked.js' },
        { src: 'plugin/markdown/markdown.js' },
        { src: 'plugin/notes/notes.js', async: true },
        //{ src: 'plugin/chalkboard/chalkboard.js' },
        //{ src: 'plugin/audio/slideshow-recorder.js', condition: function( ) { return !!document.body.classList; }},
        //{ src: 'plugin/audio/audio-slideshow.js', condition: function( ) { return !!document.body.classList; } },
        { src: 'plugin/menu/menu.js' },
        { src: 'plugin/highlight/highlight.js', async: true, callback: function() { hljs.initHighlightingOnLoad(); } }
    ],
    /*keyboard: {
        67: function() { RevealChalkboard.toggleNotesCanvas() },    // toggle notes canvas when 'c' is pressed
        66: function() { RevealChalkboard.toggleChalkboard() }, // toggle chalkboard when 'b' is pressed
        46: function() { RevealChalkboard.clear() },    // clear chalkboard when 'DEL' is pressed
         8: function() { RevealChalkboard.reset() },    // reset chalkboard data on current slide when 'BACKSPACE' is pressed
        68: function() { RevealChalkboard.download(); Recorder.downloadZip(); }, // downlad recorded chalkboard drawing when 'd' is pressed
        82: function() { Recorder.toggleRecording(); },
    },
    chalkboard: { // font-awesome.min.css must be available
        src: "c2-files/chalkboard.json",
        toggleChalkboardButton: { left: "80px" },
        toggleNotesButton: { left: "130px" },
        theme: "chalkboard",
        // configuration options for notes canvas and chalkboard
        color: [ 'rgba(0,0,255,1)', 'rgba(255,255,255,0.5)' ],
        background: [ 'rgba(127,127,127,.1)' , 'plugin/chalkboard/img/blackboard.png' ],
        pen:  [ 'plugin/chalkboard/img/boardmarker.png', 'plugin/chalkboard/img/chalk.png' ],
    },
    audio: {
        prefix: 'c1-files/',   // audio files are stored in the "audio" folder
        suffix: '.wav',
        defaultDuration: 10
    },*/
    menu: {
    // ...
        transitions: false,
        themes: false,
        custom: [
            {
                title: 'Plus',
                icon: '<i class="fa fa-bars">',
                content: '<ul style="padding: 20px;"><li><a href="'+(function() {
                    var l = ''+window.location
                    return l.split('#')[0]+'?print-pdf'
                })()+'"><i class="fa fa-print" aria-hidden="true"></i> Imprimer </a></li></ul>' }
        ]
    }
});


if(window.location.search.match( /print-pdf/gi )) {
    window.print();
}
