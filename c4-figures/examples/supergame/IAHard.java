package supergame ;

/**
* Classe d'IA difficile
* @author Quentin Bramas
* @version 1.00
*/
public class IAHard extends IA {
  private boolean visible = true ;
  /**
  * Teste la visibilité de l'IA
  * @return true si l'IA est visible
  */
  public boolean isVisible () {
    return visible ;
  }
  /** {@inheritDoc} et cache l'IA */
  @Override
  public int courir (int dist) {
    this.hide();
    return super.courir(dist);
  }
  /**
  * cache l'IA (du coup elle est vachement difficile à battre)
  */
  private void hide() {
    this.visible = false;
  }
}
            
