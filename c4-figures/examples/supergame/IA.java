package supergame ;
/**
* Classe générique pour une IA
* @see <a href="https://fr.wikipedia.org/wiki/Intelligence_artificielle">wikipedia</a>
* @author Quentin Bramas
* @version 1.00
*/
public class IA {

  int pos=0;
  /**
  * Méthode simulant la course de l'IA
  * @param dist distance parcourue par l'IA
  * @return retourne la position actuelle
  */
  public int courir (int dist) {
    return pos += dist ;
  }
}
