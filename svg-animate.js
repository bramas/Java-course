
function parseId(id) {
    var o = {};
    if(!id) return o;
    id = id.split(';');
    id.forEach(function(e) {
        var kv = e.split(':');
        if(kv.length > 1)
        {
            kv[1] = kv[1].replace(new RegExp('_x2C_', 'g'),',');
            if(kv[1].match(',')) {
                kv[1] = kv[1].split(',');
            }
        } else {
            kv.push(true);
        }
        o[kv[0]]=kv[1];
    });
    return o;
}
function filterAppearValue(x) {
    if(x.match(/\-$/)) {
        x = x.split('-');
        x = [parseInt(x[0], 10), -1];
    } else
    if(x.match(/\-/)) {
        x = x.split('-');
        x = [parseInt(x[0], 10), parseInt(x[1], 10) + 1];
    } else {
        x = parseInt(x, 10)
        x = [x, x + 1];
    }
    return x;
 }

function createAnimation(ob) {
    var time = 0;
    var maxTime = 0;
    var dynamicElements = [];

    jQuery.makeArray($(ob.contentDocument).find('path, g, text, rect')).forEach(function(p, a_i) {

        var options = parseId($(p).attr('id'));

        if(!options || !options.appear) return;
        if(typeof options.appear === 'string' ) {
            options.appear = [options.appear];
        }

        var times = options.appear.map(filterAppearValue);

        if(times[0][0] > 0)
            $(p).addClass("svg-hide-at-time-0");
        for(var i = 0; i < times.length; ++i) {
            $(p).addClass("svg-show-at-time-"+times[i][0]);
            /*if(! times[i+1] || times[i] != times[i+1]){
                $(p).addClass("svg-hide-at-time-"+(times[i]+1));
            }*/
            if(times[i][0] > maxTime) maxTime = times[i][0];
            if(times[i][1] > 0) {
                $(p).addClass("svg-hide-at-time-"+(times[i][1]));
                if(times[i][1] > maxTime) maxTime = times[i][1];
            }
        }

    });
    function nextFrame(e) {
        window.focus();
        if(time >= maxTime)
            return false;
        time++;
        console.log(time+"/"+maxTime);
        $(ob.contentDocument).find('.svg-hide-at-time-'+time).hide();
        $(ob.contentDocument).find('.svg-show-at-time-'+time).show();
        return true;

    }
    $(ob.contentDocument).click(nextFrame);
    /*$(ob.contentDocument).keyup(function(e) {
        console.log(e.keyCode);
        //if(37 >= e.keyCode && e.keyCode <= 40) {
        window.focus();
        $(document).trigger('keydown', e);
        $(document).trigger('keyup', e);
        //}
    });*/

    $(ob.contentDocument).find('.svg-hide-at-time-0').hide();
}


$('object').each(function(i, el) {
    el.addEventListener('load', createAnimation.bind(this, el), false);
});
