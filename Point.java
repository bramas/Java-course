package fr.dant.robotgame;

final public class Point {
  private final int x, y;
  public Point(int x, int y) {
    this.x = x; this.y = y;
  }
  public Point add(int dx, int dy) {
    return new Point(x+dx, y+dy);
  }
  public Point add(Point p) {
    return add(p.x, p.y);
  }
  public Point sub(int dx, int dy) {
    return new Point(x-dx, y-dy);
  }
  public Point sub(Point p) {
    return sub(p.x, p.y);
  }
  public int getX() { return x; }
  public int getY() { return y; }

  @Override
  public boolean equals(Object o) {
    if(o == null || o.getClass() != this.getClass())
      return false;
    Point p = (Point)o;
    return x == p.x && y == p.y;
  }
  @Override
  public int hashCode() {
    return 7*x + 37*y;
  }

  @Override
  public String toString() {
    return "("+x+", "+y+")";
  }
}
