import java.util.*;
import java.net.*;
import java.io.*;
class Main {

  public static void main(String[] args) throws Exception {
      String host = "java-server.bramas.fr";

      if(args.length >= 2) {
          host = args[1];
      }

      int port = Integer.parseInt(args[0]);
      Socket socket = new Socket(host, port);


      SocketAddress ici = socket.getLocalSocketAddress();
      SocketAddress labas = socket.getRemoteSocketAddress();

      System.out.println(ici+" => "+labas);

      PrintWriter out = new PrintWriter(socket.getOutputStream(), true);
      BufferedReader in = new BufferedReader(new InputStreamReader(socket.getInputStream()));
      BufferedReader stdin = new BufferedReader(new InputStreamReader(System.in));

      while(socket.isConnected()) {

          String stdLine = stdin.readLine();
          out.println(stdLine);

          String line = in.readLine();
          System.out.println(line);

          if(stdLine.equals("Bye")) break;
      }

      socket.close();




      /*out.println("");

      String line;
      while((line = in.readLine()) != null) {
          System.out.println(line);
      }
      */
  }

}
