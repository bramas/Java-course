import java.util.*;
import java.net.*;
import java.io.*;
class AsyncServer {

    public static void main(String[] args) throws Exception {

        int port = 3014;
        ServerSocket serverSocket = new ServerSocket(port);
        System.out.println("waiting for connexions in port "+port);

        while(true) {

            Socket clientSocket = serverSocket.accept();
            PrintWriter out = new PrintWriter(clientSocket.getOutputStream(), true);
            BufferedReader in =
                new BufferedReader(
                new InputStreamReader(clientSocket.getInputStream())
                );

            Runnable task = () -> {
                try {
                    String inputLine = "";
                    while ((inputLine = in.readLine()) != null) {
                            if (inputLine.equals("Bye"))
                            {
                                    out.println("Bye");
                                    break;
                            }
                            out.println("You said "+inputLine);
                    }
                    out.close();
                    in.close();
                    clientSocket.close();
                } catch(Exception e) { }
            };
            new Thread(task).start();

        }
    }
}
