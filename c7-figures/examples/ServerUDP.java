import java.util.*;
import java.net.*;
import java.io.*;
class ServerUDP {

  public static void main(String[] args) throws Exception {
        int port = 3016;
        DatagramSocket UDPSocket = new DatagramSocket(port);

        byte msg[] = new byte[2];

        char[] possibleChar = "0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ".toCharArray();

        DatagramPacket in = new DatagramPacket(msg, 2);
        while(true) {
            System.out.println("waiting at "+UDPSocket.getLocalSocketAddress();
            UDPSocket.receive(in);

            int nbMessage = msg[0];
            int responseSize = msg[1];

            for(int i = 0; i < nbMessage; i++) {
                byte[] response = new byte[responseSize+1];
                DatagramPacket out = new DatagramPacket(response, responseSize, in.getSocketAddress());
                response[0] = (byte)i;
                for(int b = 1; b < responseSize+1; b++) {
                    response[b] = (byte)possibleChar[(int)(Math.random()*possibleChar.length)];
                }
                UDPSocket.send(out);
            }

            System.out.println("sent to "+in.getSocketAddress()+" "+nbMessage+" messages");
        }
    }
}
