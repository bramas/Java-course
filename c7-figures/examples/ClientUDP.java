import java.util.*;
import java.net.*;
import java.io.*;
class ClientUDP {

  public static void main(String[] args) throws Exception {
        int port = args.length > 0 ? Integer.parseInt(args[0]) : 3016;
        InetSocketAddress host = new InetSocketAddress("java-server.bramas.fr", port);

        DatagramSocket UDPSocket = new DatagramSocket();

        int nbMessage = 120;
        int messageSize = 100;

        byte msg[] = {(byte)nbMessage, (byte)messageSize};

        DatagramPacket out = new DatagramPacket(msg, 2, host);

        UDPSocket.send(out);

        byte[] response = new byte[messageSize + 1];
        DatagramPacket in = new DatagramPacket(response, messageSize + 1);
        int lastId = -1;
        for (int i = 0; i < nbMessage; i++) {
            UDPSocket.receive(in);
            if(response[0] != lastId + 1) System.out.println("erreur");
            System.out.println(response[0]+" -> "+new String(response, 1, messageSize));
            lastId = response[0];
        }
    }
}
