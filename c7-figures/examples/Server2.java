import java.util.*;
import java.net.*;
import java.io.*;
class Server2 {

  public static void main(String[] args) throws Exception {
      int port = 3014;
        ServerSocket serverSocket = new ServerSocket(port);
        Socket clientSocket = serverSocket.accept();

        System.out.println("connection of "+clientSocket.getRemoteSocketAddress());

        PrintWriter out = new PrintWriter(clientSocket.getOutputStream(), true);
        BufferedReader in = new BufferedReader(new InputStreamReader(clientSocket.getInputStream()));


        String inputLine = "", outputLine = "";
        while ((inputLine = in.readLine()) != null) {
            outputLine = "You said "+inputLine;
            if (inputLine.equals("Bye"))
            {
                    out.println("Bye");
                    break;
            }
            out.println(outputLine);
        }
        System.out.println("disconnection of "+clientSocket.getRemoteSocketAddress());
        clientSocket.close();
        serverSocket.close();
    }
}
