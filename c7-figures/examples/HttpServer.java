import java.util.*;
import java.net.*;
import java.io.*;
class HttpServer {


  public static void main(String[] args) throws Exception {
        int port = 3015;
        ServerSocket serverSocket = new ServerSocket(port);
        System.out.println("waiting for connexions in port "+port);
        while(true) {
            Socket clientSocket = serverSocket.accept();
            PrintWriter out = new PrintWriter(clientSocket.getOutputStream(), true);
            BufferedReader in =
                new BufferedReader(
                new InputStreamReader(clientSocket.getInputStream())
                );

            System.out.println("connection of "+clientSocket.getRemoteSocketAddress());
            String inputLine = "", outputLine = "";


			while ((inputLine = in.readLine()) != null && inputLine.length() != 0) {
				System.out.println(inputLine);
                outputLine += inputLine+"\n";
            }

    		// Start sending our reply, using the HTTP 1.1 protocol
       	 	out.print("HTTP/1.1 200 \r\n"); // Version & status code
	        out.print("Content-Type: text/plain\r\n"); // The type of data
			out.print("Content-Length: "+outputLine.length()+"\r\n");
	        out.print("Connection: close\r\n"); // Will close stream
	        out.print("\r\n"); // End of headers

			out.print(outputLine);

			out.close();
			in.close();
			clientSocket.close();
            
            System.out.println("disconnection of "+clientSocket.getRemoteSocketAddress());
        }

    }

}
