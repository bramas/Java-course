# Cours de Java (2016-2017)

* [Cours 1 (Introduction)](c1-introduction.html)
* [Cours 2 (Héritage et Polymorphisme)](c2-heritage-polymorhpisme.html)
* [Cours 3 (Interfaces)](c3-interface-et-sous-typage.html)
* [Cours 4 (Fiabilité)](c4-fiabilite-et-documentation.html)
* [Cours 5 (Entrées/Sorties)](c5-entrees-sorties.html)
* [Cours 6 (Généricité et Collections)](c6-genericite-et-collections.html)
* [Cours 7 (Sockets)](c7-sockets.html)
* [Cours 8 (Programmation Concurrente)](c8-programmation-concurrente.html)
* [Cours 9 (GUI et JavaFX)](c9-gui-et-javafx.html)



[Examples du cours 7](c7-figures/examples/index.html)

[Examples du cours 8](c8-figures/examples/index.html)

[Examples du cours 9](c9-figures/examples/index.html)


## Utilisation de JavaFX avec Eclipse

Par défaut eclipse empêche l'utilisation de javaFX, pour qu'il accepte il faut :

* aller dans les propriétés du projet (clic droit sur le projet a gauche, puis propriétés)
* aller dans le menu "Java Build Path"
* aller dans l'onglet "Libraries"
* étendre la première entrée et cliquer une fois sur "Access rules"
* Cliquer sur "Edit" puis sur "Add..."
* choisir "Accessible" pour la Resolution et pour la règle mettre javafx/**
* enregistrez tout, ça devrait fonctionner

## Projet

[Sujet du projet](project_Java.pdf)
