
import javafx.application.Application;
import javafx.event.*;
import javafx.scene.input.*;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.stage.Stage;
import javafx.beans.property.*;
import javafx.scene.shape.Circle;

public class EventApp4 extends Application {

    public static void main(String[] args) {
        launch(args);
    }

    @Override
    public void start(Stage primaryStage) {
        primaryStage.setTitle("Hello World");
        Group root = new Group();

        Scene scene = new Scene(root, 300, 250, Color.LIGHTGREEN);
        VBox layout = new VBox();
        root.getChildren().add(layout);

        Slider slider = new Slider();
        slider.valueProperty().addListener((obs, oldValue, newValue) -> {
            System.out.println(oldValue+" -> "+newValue);
        });

        layout.getChildren().add(slider);

        Slider slider2 = new Slider();
        slider.valueProperty().bind(slider2.valueProperty().negate().add(100));
        layout.getChildren().add(slider2);

        primaryStage.setScene(scene);
        primaryStage.show();
    }
}
