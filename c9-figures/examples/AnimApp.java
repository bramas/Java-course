
import javafx.application.Application;
import javafx.event.*;
import javafx.scene.input.*;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.paint.Color;
import javafx.stage.Stage;
import javafx.animation.*;
import javafx.util.Duration;
import javafx.scene.shape.*;
import javafx.scene.transform.*;
 import javafx.scene.image.*;

public class AnimApp extends Application {

    public static void main(String[] args) {
        launch(args);
    }

    @Override
    public void start(Stage primaryStage) {
        primaryStage.setTitle("Hello World");
        Group root = new Group();

        Scene scene = new Scene(root, 400, 400, Color.LIGHTGREEN);

        ImageView chat = new ImageView(new Image("chat.jpg"));
        chat.setFitHeight(400);
        chat.setPreserveRatio(true);
        chat.setTranslateX(16);
        chat.setTranslateY(2);
        root.getChildren().add(chat);

        Button btn = new Button();
        btn.setLayoutX(100);
        btn.setLayoutY(80);
        btn.setText("Hello World");
        btn.setOnAction((event) -> {
            System.out.println("Hello World");
        });
        root.getChildren().add(btn);

        Rectangle square = new Rectangle();
        square.setX(100);//réglage de la position, de la taille et de la couleur du cercle
        square.setY(200);
        square.setWidth(50);
        square.setHeight(50);
        square.setFill(Color.YELLOW);
        square.setStroke(Color.ORANGE);//réglage de la couleur de la bordure et de son épaisseur
        square.setStrokeWidth(5);
        root.getChildren().add(square);

        Rotate r = new Rotate(0, 0, 0);
        btn.getTransforms().add(r);




        Timeline timeline = new Timeline();
        timeline.getKeyFrames().addAll(
            new KeyFrame(Duration.ZERO, new KeyValue(square.xProperty(), 0)),
            new KeyFrame(Duration.ZERO, new KeyValue(r.angleProperty(), -10)),
            new KeyFrame(new Duration(1000), new KeyValue(square.xProperty(), 100)),
            new KeyFrame(new Duration(1000), new KeyValue(r.angleProperty(), 10))
        );
        timeline.setAutoReverse(true);
        timeline.setCycleCount(Timeline.INDEFINITE);
        timeline.play();



        primaryStage.setScene(scene);
        primaryStage.show();
    }
}
