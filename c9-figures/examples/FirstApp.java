import javafx.application.Application;
import javafx.stage.Stage;


public class FirstApp extends Application{

    public static void main(String[] args) {
        System.out.println( "Main method inside Thread : " +  Thread.currentThread().getName());
        launch(args);
    }

    @Override
    public void start(Stage primaryStage) throws Exception {
        System.out.println( "Start method inside Thread : " +  Thread.currentThread().getName());
        primaryStage.setTitle("Hello World!");
        primaryStage.show();
    }
}
