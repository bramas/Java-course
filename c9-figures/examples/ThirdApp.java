
import javafx.application.Application;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.paint.Color;
import javafx.stage.Stage;

import javafx.scene.layout.BorderPane;

import javafx.scene.shape.Circle;

public class ThirdApp extends Application {

    public static void main(String[] args) {
        launch(args);
    }

    @Override
    public void start(Stage primaryStage) {
        primaryStage.setTitle("Hello World");
        Group root = new Group();

        Scene scene = new Scene(root, 400, 250, Color.LIGHTGREEN);

        BorderPane layout = new BorderPane();
        root.getChildren().add(layout);

        Button btn = new Button();
        btn.setText("Hello World");
        layout.setRight(btn);

        Circle cercle = new Circle();

        cercle.setRadius(100);
        cercle.setFill(Color.YELLOW);
        cercle.setStroke(Color.ORANGE);//réglage de la couleur de la bordure et de son épaisseur
        cercle.setStrokeWidth(5);
        layout.setBottom(cercle);//on ajoute le cercle au groupe root

        Button btn2 = new Button();
        btn2.setText("Hello World2");
        layout.setTop(btn2);

        primaryStage.setScene(scene);
        primaryStage.show();
    }
}
