
import javafx.application.Application;
import javafx.event.*;
import javafx.scene.input.*;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.paint.Color;
import javafx.stage.Stage;

import javafx.scene.shape.Circle;

public class EventApp3 extends Application {

    public static void main(String[] args) {
        launch(args);
    }

    @Override
    public void start(Stage primaryStage) {
        primaryStage.setTitle("Hello World");
        Group root = new Group();

        Scene scene = new Scene(root, 300, 250, Color.LIGHTGREEN);

        Button btn = new Button();
        btn.setLayoutX(100);
        btn.setLayoutY(80);
        btn.setText("Hello World");
        btn.setOnAction((event) -> {
            System.out.println("Hello World");
        });
        root.getChildren().add(btn);

        Group cercles = new Group();

        cercles.setOnMouseEntered((e) -> {
            System.out.println("Entered group "+e.getX()+","+e.getY());
        });
        cercles.setOnMouseExited((e) -> {
            System.out.println("Exited group "+e.getX()+","+e.getY());
        });
        cercles.setOnMouseMoved((e) -> {
            System.out.println("Moved group "+e.getX()+","+e.getY());
        });

        root.getChildren().add(cercles);

        Circle cercle1 = new Circle();
        cercle1.setCenterX(300);//réglage de la position, de la taille et de la couleur du cercle
        cercle1.setCenterY(200);
        cercle1.setRadius(100);
        cercle1.setFill(Color.YELLOW);
        cercle1.setStroke(Color.ORANGE);//réglage de la couleur de la bordure et de son épaisseur
        cercle1.setStrokeWidth(5);
        cercle1.setOnMouseEntered((e) -> {
            System.out.println("Entered1 cercle "+e.getX()+","+e.getY());
        });
        cercle1.setOnMouseExited((e) -> {
            System.out.println("Exited1 cercle "+e.getX()+","+e.getY());
        });
        cercle1.setOnMouseMoved((e) -> {
            System.out.println("Moved1 cercle "+e.getX()+","+e.getY());
        });
        cercles.getChildren().add(cercle1);//on ajoute le cercle au groupe root


        Circle cercle2 = new Circle();
        cercle2.setCenterX(300);//réglage de la position, de la taille et de la couleur du cercle
        cercle2.setCenterY(200);
        cercle2.setRadius(50);
        cercle2.setFill(Color.YELLOW);
        cercle2.setStroke(Color.ORANGE);//réglage de la couleur de la bordure et de son épaisseur
        cercle2.setStrokeWidth(5);
        cercle2.setOnMouseEntered((e) -> {
            System.out.println("Entered2 cercle "+e.getX()+","+e.getY());
        });
        cercle2.setOnMouseExited((e) -> {
            System.out.println("Exited2 cercle "+e.getX()+","+e.getY());
        });
        cercle2.setOnMouseMoved((e) -> {
            System.out.println("Moved2 cercle "+e.getX()+","+e.getY());
        });

        cercles.getChildren().add(cercle2);//on ajoute le cercle au groupe root

        primaryStage.setScene(scene);
        primaryStage.show();
    }
}
