
import javafx.application.Application;
import javafx.event.*;
import javafx.scene.input.*;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.paint.Color;
import javafx.stage.Stage;

import javafx.scene.shape.Circle;

public class EventApp2 extends Application {

    public static void main(String[] args) {
        launch(args);
    }

    @Override
    public void start(Stage primaryStage) {
        primaryStage.setTitle("Hello World");
        Group root = new Group();

        Scene scene = new Scene(root, 300, 250, Color.LIGHTGREEN);

        Button btn = new Button();
        btn.setLayoutX(100);
        btn.setLayoutY(80);
        btn.setText("Hello World");
        btn.setOnAction((event) -> {
            System.out.println("Hello World");
        });
        root.getChildren().add(btn);

        Circle cercle = new Circle();
        cercle.setCenterX(300);//réglage de la position, de la taille et de la couleur du cercle
        cercle.setCenterY(200);
        cercle.setRadius(100);
        cercle.setFill(Color.YELLOW);
        cercle.setStroke(Color.ORANGE);//réglage de la couleur de la bordure et de son épaisseur
        cercle.setStrokeWidth(5);
        cercle.setOnMousePressed((e) -> {
            System.out.println("pressed cercle "+e.getX()+","+e.getY());
        });
        cercle.setOnMouseClicked((e) -> {
          cercle.requestFocus();
            System.out.println("clicked cercle "+e.getX()+","+e.getY());
        });
        cercle.setOnMouseReleased((e) -> {
            System.out.println("released cercle "+e.getX()+","+e.getY());
        });
        cercle.setOnMouseEntered((e) -> {
          cercle.setFill(Color.ORANGE);
        });
        cercle.setOnMouseExited((e) -> {
          cercle.setFill(Color.YELLOW);
        });

        root.setOnKeyPressed(e -> System.out.println(e.getText()));

        root.getChildren().add(cercle);//on ajoute le cercle au groupe root

        primaryStage.setScene(scene);
        primaryStage.show();
    }
}
