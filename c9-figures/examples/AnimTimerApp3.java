
import javafx.application.Application;
import javafx.event.*;
import javafx.scene.input.*;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.paint.Color;
import javafx.stage.Stage;
import javafx.animation.*;
import javafx.util.Duration;
import javafx.scene.shape.*;
import javafx.scene.transform.*;
 import javafx.scene.image.*;
 import javafx.scene.input.KeyCode;
 import java.util.*;

public class AnimTimerApp3 extends Application {

    private long lastUpdate = 0;
    private Set<KeyCode> keyPressed = new HashSet<KeyCode>();
    @Override public void start(Stage stage) {
        stage.setTitle("Hello World");
        Group root = new Group();

        Scene scene = new Scene(root, 400, 400, Color.WHITE);

        ImageView chat = new ImageView(new Image("nyan.jpg"));
        chat.setFitHeight(40);
        chat.setPreserveRatio(true);
        chat.setTranslateX(16);
        chat.setTranslateY(2);

        root.getChildren().add(chat);


        root.setOnKeyPressed((e) -> {
          keyPressed.add(e.getCode());
        });
        root.setOnKeyReleased((e) -> {
          keyPressed.remove(e.getCode());
        });
        root.requestFocus();

        double speed = 50; // pixels/secondes
        lastUpdate = System.nanoTime();
        AnimationTimer timer = new AnimationTimer() {
            public void handle(long now)
            {
                //if(lastUpdate == 0) lastUpdate = now;

                long elapsed = now - lastUpdate;
                lastUpdate = now;

                int deltaX = 0, deltaY = 0;
                for(KeyCode c: keyPressed)
                {
                  switch(c) {
                    case UP:
                      deltaY = -100;
                      break;
                    case DOWN:
                      deltaY = 100;
                      break;
                    case LEFT:
                      deltaX = -100;
                      break;
                    case RIGHT:
                      deltaX = 100;
                      break;
                    default: break;
                  }
                }

                chat.setTranslateY(chat.getTranslateY() + deltaY*elapsed/1000000000.0);
                chat.setTranslateX(chat.getTranslateX() + deltaX*elapsed/1000000000.0);
            }
        };
        timer.start();
        stage.setScene(scene);
        stage.show();

    }


    public static void main(String[] args) {
        Application.launch(args);
    }
  }
