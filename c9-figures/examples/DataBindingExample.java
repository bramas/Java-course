import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.*;
import javafx.beans.value.*;


class DataBindingExample {
  public static void main(String[] args) {
    SimpleStringProperty nom = new SimpleStringProperty();
    nom.set("Alice");
    System.out.println(nom);

    SimpleIntegerProperty a = new SimpleIntegerProperty();
    SimpleIntegerProperty b = new SimpleIntegerProperty();
    b.bind(a.add(3).multiply(2));

    a.set(5);
    System.out.println(b.get());
    a.set(10);
    System.out.println(b.get());

    b.addListener((obs, oldValue, newValue) -> {
        System.out.println("'b' vaut maintenant "+newValue);
    });
    a.set(2);
    a.set(3);
    a.set(4);
  }
}
